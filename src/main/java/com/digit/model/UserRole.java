/**
 * 
 */
package com.digit.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author rajesh.kumar
 *
 */


@Data
@Entity
@Table(name="t_user_role", schema = "digit_authorization")
public class UserRole {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "digit_authorization.t_user_role_user_role_id_seq")
	@SequenceGenerator(name = "digit_authorization.t_user_role_user_role_id_seq", sequenceName = "digit_authorization.t_user_role_user_role_id_seq", allocationSize = 1)
	
	@Column(name = "user_role_id", nullable = false)
	private Long userRoleId;
	
	@Column(name = "user_id", nullable = false)
	private Long userId;
	
	@Column(name = "role_id", nullable = false)
	private Long roleId;
	
	@Column(name = "active")
	private boolean active;
	
	@Column(name = "created_user")
	private Long  createdUser;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@CreationTimestamp
	@Column(name = "created_date")
	private Timestamp createdDate;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@UpdateTimestamp
	@Column(name = "last_modified_date")
	private Timestamp lastModifiedDate;
	
	
	@Column(name = "last_modified_user")
	private Long lastModifiedUser;
	
}
