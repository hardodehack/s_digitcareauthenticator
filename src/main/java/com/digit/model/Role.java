package com.digit.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name="t_role", schema = "digit_authorization")
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "digit_authorization.t_role_role_id_seq")
	@SequenceGenerator(name = "digit_authorization.t_role_role_id_seq", sequenceName = "digit_authorization.t_role_role_id_seq", allocationSize = 1)
	
	@Column(name = "role_id", nullable = false)
	private Long roleId;
	
	@Column(name = "role_name", nullable = false)
	private String roleName;
	
	@Column(name = "active", nullable = false)
	private Boolean active;
	
	@Column(name = "created_user", nullable = false)
	private Long createdUser;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Column(name="created_date")
	@CreationTimestamp
	private Timestamp createdDate;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name="last_modified_date")	
	@UpdateTimestamp
	private Timestamp lastModifiedDate;
	
	@Column(name="last_modified_user")
	private Long lastModifiedUser;
	
}
