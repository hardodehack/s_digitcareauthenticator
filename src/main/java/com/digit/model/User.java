package com.digit.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author hardik.patel
 *
 */

@Data
@Entity
@Table(name = "t_user", schema = "digit_authorization")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "digit_authorization.t_user_user_id_seq")
	@SequenceGenerator(name = "digit_authorization.t_user_user_id_seq", sequenceName = "digit_authorization.t_user_user_id_seq", allocationSize = 1)
	@Column(name = "user_id")
	private Long userId;

	@Column(name = "user_name")
	private String userName;

	private Integer type;

	private String login;

	private String password;

	@Column(name = "api_key")
	private String apiKey;

	private Boolean active;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "created_date")
	@CreationTimestamp
	private Timestamp createdDate;

	@Column(name = "created_user")
	private Long createdUser;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "last_modified_date")
	@UpdateTimestamp
	private Timestamp lastModifiedDate;

	@Column(name = "last_modified_user")
	private Long lastModifiedUser;

}
