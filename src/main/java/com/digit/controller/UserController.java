/**
 * 
 */
package com.digit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digit.model.User;
import com.digit.repository.UserRepository;
import com.digit.service.UserDetailsServiceImpl;

/**
 * @author hardik.patel
 *
 */

@RestController
public class UserController {

	@Autowired
	UserRepository userRepo;

	@Autowired
	UserDetailsServiceImpl userService;
	

	@GetMapping("/users/{name}")
	public User getUserDetailsByName(@PathVariable("name") String name) {

		return userRepo.findByUsername(name);

	}

	@PostMapping("/users")
	public ResponseEntity<?> createNewUser(@RequestParam("userName") String userName,
			@RequestParam("login") String login, @RequestParam("password") String password) {
		return userService.creatNewUser(userName, login, password);
	}
		
}