/**
 * 
 */
package com.digit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digit.repository.RoleRepository;
import com.digit.repository.UserRepository;
import com.digit.service.RoleDetailsServiceImpl;

/**
 * @author rajesh.kumar
 *
 */
@RestController
public class RoleController {
	
	@Autowired
	RoleDetailsServiceImpl roleService;
	
	@Autowired
	UserRepository userRepo;
	@Autowired
	RoleRepository roleRepo;
	
	@PostMapping("/role")
	public ResponseEntity<?> createRole(@RequestParam("roleName") String roleName){
	
	return roleService.createNewRole(roleName);

}
	
	@DeleteMapping("/role/{id}")
	public ResponseEntity<?> deleteRole(@PathVariable(value = "id") String roleId) {

		return roleService.deleteRole(roleId);
	}

	@GetMapping("/getRoles")
	public ResponseEntity<?> getRoles() {

		return roleService.getRoles();

	}
}
