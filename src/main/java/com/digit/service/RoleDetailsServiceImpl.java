/**
 * 
 */
package com.digit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.digit.exception.ApiException;
import com.digit.model.Role;
import com.digit.repository.RoleRepository;

/**
 * @author rajesh.kumar
 *
 */

@Service
public class RoleDetailsServiceImpl {

	@Autowired
	RoleRepository roleRepository;
	
	public ResponseEntity<?> createNewRole(String roleName) {

		Role role = new Role();
		role.setRoleName(roleName);
		role.setActive(true);
		role.setCreatedUser(1L);
		role.setLastModifiedUser(1L);

		try {
			roleRepository.saveAndFlush(role);
		} catch (DataIntegrityViolationException e) {

			ApiException apiExp = new ApiException(HttpStatus.BAD_REQUEST, "Constraint Violation.",
					"Role Already Exist.");
			return new ResponseEntity<Object>(apiExp, new HttpHeaders(), apiExp.getStatus());
		} catch (Exception e1) {
			ApiException apiExp = new ApiException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Server Error while Saving or Retriving Role.");
			return new ResponseEntity<Object>(apiExp, new HttpHeaders(), apiExp.getStatus());
		}

		return ResponseEntity.status(HttpStatus.OK).body(role);
	}

	public ResponseEntity<?> deleteRole(String roleId) {

		try {
			long id = Long.parseLong(roleId);
			roleRepository.delete(id);
		} catch (Exception e) {
			ApiException apiExp = new ApiException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Server Error while deleting Role.");
			return new ResponseEntity<Object>(apiExp, new HttpHeaders(), apiExp.getStatus());

		}
		return ResponseEntity.status(HttpStatus.OK).body("deleted role id - " + roleId);
	}
	
	public ResponseEntity<?> getRoles(){
		List<Role> roleDetails = null;
		try {
		 roleDetails = roleRepository.getAllRoles();
		
		}catch (Exception e) {
			ApiException apiExp = new ApiException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Server Error while Retriving Role.");
			return new ResponseEntity<Object>(apiExp, new HttpHeaders(), apiExp.getStatus());
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(roleDetails);
	}
	
}
