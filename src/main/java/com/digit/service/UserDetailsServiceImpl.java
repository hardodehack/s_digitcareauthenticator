/**
 * 
 */
package com.digit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.digit.exception.ApiException;
import com.digit.model.User;
import com.digit.repository.UserRepository;
import com.digit.util.RandomStringUtil;

/**
 * @author hardik.patel
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	RandomStringUtil randomStringUtil;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);

		if (user != null) {
			return (UserDetails) user;
		}
		throw new UsernameNotFoundException(username);
	}

	public ResponseEntity<?> creatNewUser(String userName, String login, String password) {

		User user = new User();
		User newUser = null;
		user.setUserName(userName);
		user.setLogin(login);
		user.setPassword(password);
		user.setApiKey(randomStringUtil.randomAlphaNumeric(32));
		user.setType(1);
		user.setActive(true);
		user.setCreatedUser(1L);
		user.setLastModifiedUser(1L);

		try {
			userRepository.saveAndFlush(user);
			newUser = userRepository.findByUsername(userName);
		} catch (DataIntegrityViolationException e) {

			ApiException apiExp = new ApiException(HttpStatus.BAD_REQUEST, "Constraint Violation.",
					"User Already Exist with same Login or Api Key.");
			return new ResponseEntity<Object>(apiExp, new HttpHeaders(), apiExp.getStatus());

		} catch (Exception e1) {
			ApiException apiExp = new ApiException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Server Error while Saving or Retriving User.");
			return new ResponseEntity<Object>(apiExp, new HttpHeaders(), apiExp.getStatus());
		}

		return ResponseEntity.status(201).body(newUser);

	}

}
