/**
 * 
 */
package com.digit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digit.model.User;

/**
 * @author hardik.patel
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("SELECT DISTINCT user FROM User user " + "WHERE user.userName LIKE CONCAT('%',:userName,'%')")
	User findByUsername(@Param("userName") String userName);
	
	}