/**
 * 
 */
package com.digit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.digit.model.Role;

/**
 * @author rajesh.kumar
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	@Query("SELECT role FROM Role role")
	public List<Role> getAllRoles();
}
