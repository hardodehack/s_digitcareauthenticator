package com.digit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Digitauthenticator {

	public static void main(String[] args) {
		SpringApplication.run(Digitauthenticator.class, args);
	}
}
