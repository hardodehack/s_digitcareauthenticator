package com.digit.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author hardik.patel
 *
 */

@Data
public class ApiException {

	private String error;
	private String message;
	private HttpStatus status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date timestamp;

	private ApiException() {
		timestamp = new Date();
	}

	public ApiException(HttpStatus status, String message) {
		this();
		this.status = status;
		this.error = "Exception Occured.";
		this.message = message;
	}

	public ApiException(HttpStatus status, String error, String message) {
		this();
		this.status = status;
		this.error = error;
		this.message = message;
	}

}
